# coding: utf8
import json
from social.backends.yandex import YandexOAuth2 as Base


class YandexOAuth2(Base):
    ''' клиент к api яндекса Именно в этом классе добавляем методы для 
        получения данных с яндекс директа '''
    # API_URL = 'https://api.direct.yandex.ru/live/v4/json/'  # боевой сервер
    API_URL = 'https://api-sandbox.direct.yandex.ru/v4/json/'  # песочница

    def make_request(self, method_name, soc_user, params):
        ''' Создает правильный запрос на сервер яндекса '''
        data = {'method': method_name,
                'param': params,
                'token': soc_user.extra_data['access_token'],
        }
        return self.get_json(self.API_URL, 'POST', data=json.dumps(data, ensure_ascii=False))

    def get_clients_list(self, soc_user):
        ''' Возвращает учетные записи всех клиентов рекламного агентства. '''
        params = { 'Filter': {'StatusArch': 'No'}}
        return self.make_request('GetClientsList',
                                 soc_user,
                                 params)

    def get_client_info(self, soc_user, usernames=[]):
        """ Возвращает учетные записи в Яндекс.Директе. """
        if not usernames:
            usernames = [soc_user.extra_data['login']]
        return self.make_request('GetClientInfo',
                                 soc_user,
                                 usernames)

    def get_client_units(self, soc_user, usernames=[]):
        """ Возвращает количество баллов у пользователей. """
        if not usernames:
            usernames = [soc_user.extra_data['login']]
        return self.make_request('GetClientsUnits',
                                 soc_user,
                                 usernames)

    def get_campaings_list(self,soc_user, usernames=[]):
        """ Возвращает список кампаний """
        if not usernames:
            usernames = [soc_user.extra_data['login']]
        return self.make_request('GetCampaignsList',
                                 soc_user,
                                 usernames)

    def get_banners(self,soc_user, campaing_ids):
        """ Возвращает список кампаний """
        params = {'CampaignIDS': campaing_ids}
        return self.make_request('GetBanners',
                                 soc_user,
                                 params)
