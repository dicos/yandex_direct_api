# README #

Данный проект является прототипом системы сбора и обрабоки информации о рекламных акциях. На текущий момент (18.02.2015) реализована работа с api Яндекс.Директ.

### Начало работы ###

* создать виртуальное окружение командой virtualenv yandex_direct_api_
* войти в каталог yandex_direct_api_, выполнив команду cd yandex_direct_api_
* git clone git@bitbucket.org:dicos/yandex_direct_api.git
* cd yandex_direct_api
* source ../bin/activate
* pip install -r requirements.pip
* cp yandex_direct_api/settings/local.py.example yandex_direct_api/settings/local.py
* открыть файл yandex_direct_api/settings/local.py  в текстовом редакторе и создать свой SECRET_KEY, а также  написать параметры SOCIAL_AUTH_YANDEX_OAUTH2_KEY и SOCIAL_AUTH_YANDEX_OAUTH2_SECRET
* python manage.py migrate
* python manage.py runserver

### Что умеет система ###

* авторизироваться на яндексе;
* показывать список клиентов
* подробную информацию о клиенте
* список кампаний
* список баннеров кампании