# coding: utf8
from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'yandex_direct_api.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    (r'^$', 'interface.views.index'),
    url(r'get_^code/$', 'interface.views.get_code',
        name='get_code'),
    url(r'^get_token/$', 'interface.views.get_token',
        name='get_token'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='login', kwargs={'template_name': 'interface/login.html'}),
    (r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'template_name': 'interface/logout.html'}),
    url(r'^admin/', include(admin.site.urls)),

    # Отчеты
    url(r'^reports/client/list/(?P<backend>[^/]+)/$',
        'interface.views.clients_list',
        name='clients_list'),
    url(r'^reports/client/list/(?P<backend>[^/]+)/(?P<username>[^/]+)/$',
        'interface.views.client_info',
        name='client_info'),

    # баннеры, компании
    url(r'^campaigns/list/(?P<backend>[^/]+)/(?P<username>[^/]+)/$',
        'interface.views.campaigns_list',
        name='campaigns_list'),
    url(r'^campaigns/list/(?P<backend>[^/]+)/(?P<username>[^/]+)/(?P<campaing_id>\d+)/$',
        'interface.views.banners',
        name='banners'),

    url('', include('social.apps.django_app.urls', namespace='social')),
)
