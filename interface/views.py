# coding: utf8
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.conf import setting
from django.views.decorators.csrf import csrf_exempt
from django.http import Http404
import requests


@login_required
def get_code(request):
    """ Отправить за кодом подтверждения пользователя """
    url = setting.YANDEX_OAUTH_URL_GET_CODE % setting.AUTH_YANDEX_OAUTH2_ID
    return redirect(url)


@login_required
def get_token(request):
    if 'error' in request.GET:
        return Http404(request.GET['error_description'])
    return redirect(setting.LOGIN_REDIRECT_URL)

@login_required
def index(request):
    ''' просто страница-пустышка '''
    data = {'available_backends': load_backends(('backends.yandex.YandexOAuth2',))}
    return render(request, 'interface/index.html', data)


@login_required
def clients_list(request, backend, *args, **kwargs):
    ''' Список клиентов '''
    request = update_request(request, backend)
    soc_user = request.user.social_auth.get(provider=backend)
    data = {'clients': request.backend.get_clients_list(soc_user)['data'],
            'backend': backend}
    return render(request, 'interface/reports/clients_list.html', data)


@login_required
def client_info(request, backend, username, *args, **kwargs):
    ''' Список клиентов '''
    request = update_request(request, backend)
    soc_user = request.user.social_auth.get(provider=backend)
    data = {'client': request.backend.get_client_info(soc_user, [username])['data'][0],
            'units': request.backend.get_client_units(soc_user, [username])['data'][0],
            'backend': backend}
    return render(request, 'interface/reports/client_info.htnml', data)


@login_required
def campaigns_list(request, backend, username, *args, **kwargs):
    ''' Список кампаний '''
    request = update_request(request, backend)
    soc_user = request.user.social_auth.get(provider=backend)
    data = {'campaings': request.backend.get_campaings_list(soc_user, [username])['data'],
            'backend': backend,
            'username': username}
    return render(request, 'interface/campaings/campaings_list.html', data)


@login_required
def banners(request, backend, username, campaing_id, *args, **kwargs):
    ''' Список баннеров '''
    request = update_request(request, backend)
    soc_user = request.user.social_auth.get(provider=backend)
    data = {'banners': request.backend.get_banners(soc_user, [campaing_id])['data'],
            'backend': backend,
            'username': username}
    return render(request, 'interface/campaings/banners.html', data)
